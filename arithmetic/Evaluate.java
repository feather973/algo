import java.util.Stack;

// https://stackoverflow.com/questions/16207427/compile-error-cannot-find-symbol-in-stdin-and-stdout
// https://www.baeldung.com/jar-file-get-class-names (jar tf stdlib-package.jar)
import edu.princeton.cs.introcs.StdIn;
import edu.princeton.cs.introcs.StdOut;

public class Evaluate {
	public static void main(String[] args) {
		Stack<String> ops = new Stack<String>();
		Stack<Double> vals = new Stack<Double>();

		while (!StdIn.isEmpty()) {
			String s = StdIn.readString();
			StdOut.printf("[TEST] %s\n", s);

			if (s.equals("("))					;
			else if (s.equals("+"))				ops.push(s);
			else if (s.equals("-"))				ops.push(s);
			else if (s.equals("*"))				ops.push(s);
			else if (s.equals("/"))				ops.push(s);
			else if (s.equals("sqrt"))			ops.push(s);
			else if (s.equals(")")) {
				String op = ops.pop();
				double v = vals.pop();

				if (op.equals("+"))				v = vals.pop() + v;
				else if (op.equals("-"))		v = vals.pop() - v;		/// hmm..
				else if (op.equals("*"))		v = vals.pop() * v;
				else if (op.equals("/"))		v = vals.pop() / v;
				else if (op.equals("sqrt"))		v = Math.sqrt(v);

				vals.push(v);
			}
			else
				vals.push(Double.parseDouble(s));
		}

		StdOut.printf("[TEST] push done\n");
		StdOut.println(vals.pop());
	}
}
